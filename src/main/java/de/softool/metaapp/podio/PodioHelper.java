package de.softool.metaapp.podio;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.podio.APIApplicationException;
import com.podio.APIFactory;
import com.podio.ResourceFactory;
import com.podio.app.AppAPI;
import com.podio.app.Application;
import com.podio.app.ApplicationField;
import com.podio.app.ApplicationFieldType;
import com.podio.filter.FilterByValue;
import com.podio.filter.SortBy;
import com.podio.filter.StandardSortBy;
import com.podio.hook.HookAPI;
import com.podio.item.FieldValuesView;
import com.podio.item.Item;
import com.podio.item.ItemAPI;
import com.podio.item.ItemBadge;
import com.podio.item.ItemsResponse;
import com.podio.item.filter.Filter;
import com.podio.oauth.OAuthClientCredentials;
import com.podio.oauth.OAuthUsernameCredentials;
import com.podio.org.OrgAPI;
import com.podio.org.Organization;
import com.podio.org.OrganizationWithSpaces;
import com.podio.space.Space;
import com.podio.space.SpaceAPI;
import com.podio.space.SpaceMini;
import com.podio.user.UserAPI;

/**
 * Use PodioHelper.fromEnvironment() to create an instance of PodioHelper. You then have an authenticated
 * PodioHelper instance and get to the "native" Podio API using any of the getAppApi(), getSpaceApi() etc. methods.
 * 
 * Important: fromEnvironment() expects four environment variables to be set correctly in the Eclipse run-config.
 * See SYSCAR_CLIENT_ID etc. for their names or refer to documentation.
 */
public class PodioHelper {

	public static final String SYSVAR_CLIENT_ID = "podio.client.id";
	public static final String SYSVAR_CLIENT_SECRET = "podio.client.secret";
	public static final String SYSVAR_PODIO_USER = "podio.user";
	public static final String SYSVAR_PODIO_PASSWORD = "podio.password";

	private static final Logger logger = LoggerFactory.getLogger(PodioHelper.class);

	public abstract class AbstractFieldSupport {

		protected FieldValuesView view;

		public AbstractFieldSupport(FieldValuesView view) {
			this.view = view;
		}

	}

	public class ReferenceFieldSupport extends AbstractFieldSupport {

		public ReferenceFieldSupport(FieldValuesView view) {
			super(view);
		}

		public List<Integer> getValues() {
			List<Integer> vals = new ArrayList<Integer>();
			List<Map<String, ?>> values = view.getValues();
			for (Map<String, ?> map : values) {
				int appItemId = (Integer) ((Map)map.get("value")).get("item_id");

				vals.add(appItemId);
			}
			return vals;
		}

	}

	public class CategoryFieldSupport extends AbstractFieldSupport {

		public CategoryFieldSupport(FieldValuesView view) {
			super(view);
		}

		public String getValue() {
			List<Integer> vals = new ArrayList<Integer>();
			List<Map<String, ?>> values = view.getValues();
			for (Map<String, ?> map : values) {
				return ((String) ((Map)map.get("value")).get("text"));
			}
			return null;
		}

	}

	public class TextFieldSupport extends AbstractFieldSupport {

		public TextFieldSupport(FieldValuesView view) {
			super(view);
		}

		public String getValue() {
			return (String) view.getValues().get(0).get("value");
		}

	}

	private APIFactory factory;
	private ItemAPI itemApi;
	private AppAPI appApi;
	private SpaceAPI spaceApi;
	private OrgAPI orgApi;
	private UserAPI userApi;
	private HookAPI hookApi;

	/**
	 * Creates a PodioHelper using the environment varialbes for client id, secret etc.
	 * 
	 * @return PodioHelper
	 */
	public static final PodioHelper fromEnvironmentConfig() {
		String clientID = getAsString(SYSVAR_CLIENT_ID);
		String clientSecret = getAsString(SYSVAR_CLIENT_SECRET);
		String user = getAsString(SYSVAR_PODIO_USER);
		String password = getAsString(SYSVAR_PODIO_PASSWORD);

		StringBuilder msg = new StringBuilder();
		if (clientID == null) { msg.append("Missing environment variable " + SYSVAR_CLIENT_ID + "\n"); }
		if (clientSecret == null) { msg.append("Missing environment variable " + SYSVAR_CLIENT_SECRET + "\n"); }
		if (user == null) { msg.append("Missing environment variable " + SYSVAR_PODIO_USER + "\n"); }
		if (password == null) { msg.append("Missing environment variable " + SYSVAR_PODIO_PASSWORD + "\n"); }
		if (msg.length() > 0) {
			throw new IllegalArgumentException(msg.toString());
		}
		return PodioHelper.auth(clientID, clientSecret, user, password);
	}

	private static String getAsString(String name) {
		return System.getenv(name);
	}

	public static final PodioHelper auth(String clientID, String clientSecret, String user, String password) {
		if (clientID == null) { throw new IllegalArgumentException("clientID cannot be null"); }
		if (clientSecret == null) { throw new IllegalArgumentException("clientSecret cannot be null"); }
		if (user == null) { throw new IllegalArgumentException("user cannot be null"); }
		if (password == null) { throw new IllegalArgumentException("passsword cannot be null"); }
		ResourceFactory resourceFactory = new ResourceFactory(new OAuthClientCredentials(clientID, clientSecret),
				new OAuthUsernameCredentials(user, password));
		return new PodioHelper(new APIFactory(resourceFactory));
	}

	public static PodioHelper auth(String apiHost, String fileHost,
			int port, boolean ssl, boolean dryRun, String clientID,
			String clientSecret, String user, String password) {
		if (clientID == null) { throw new IllegalArgumentException("clientID cannot be null"); }
		if (clientSecret == null) { throw new IllegalArgumentException("clientSecret cannot be null"); }
		if (user == null) { throw new IllegalArgumentException("user cannot be null"); }
		if (password == null) { throw new IllegalArgumentException("passsword cannot be null"); }
		ResourceFactory resourceFactory = new ResourceFactory(apiHost, fileHost, ssl ? 443 : 80, true, dryRun, new OAuthClientCredentials(clientID, clientSecret),
				new OAuthUsernameCredentials(user, password));
		return new PodioHelper(new APIFactory(resourceFactory));
	}

	public PodioHelper(APIFactory factory) {
		this.factory = factory;
		this.itemApi = factory.getAPI(ItemAPI.class);
		this.appApi = factory.getAPI(AppAPI.class);
		this.spaceApi = factory.getAPI(SpaceAPI.class);
		this.orgApi = factory.getAPI(OrgAPI.class);
		this.userApi = factory.getAPI(UserAPI.class);
		this.hookApi = factory.getAPI(HookAPI.class);
	}

	/**
	 * Returns the "native" Podio API
	 * 
	 * @return
	 */
	public APIFactory getFactory() {
		return factory;
	}

	/**
	 * Returns the "native" Podio API
	 * 
	 * @return
	 */
	public AppAPI getAppApi() {
		return appApi;
	}

	/**
	 * Returns the "native" Podio API
	 * 
	 * @return
	 */
	public ItemAPI getItemApi() {
		return itemApi;
	}

	/**
	 * Returns the "native" Podio API
	 * 
	 * @return
	 */
	public OrgAPI getOrgApi() {
		return orgApi;
	}

	/**
	 * Returns the "native" Podio API
	 * 
	 * @return
	 */
	public SpaceAPI getSpaceApi() {
		return spaceApi;
	}

	/**
	 * Returns the "native" Podio API
	 * 
	 * @return
	 */
	public UserAPI getUserApi() {
		return userApi;
	}

	/**
	 * Returns the "native" Podio API
	 * 
	 * @return
	 */
	public HookAPI getHookApi() {
		return hookApi;
	}


	// return a list of simple String values. one value for each item in the
	// list
	public List<String> getStringValues(Application application, String fieldName) {
		ItemsResponse res = itemApi.getItems(application.getId(), 500, 0, null, false);
		List<ItemBadge> items = res.getItems();
		List<String> values = new ArrayList<String>();
		int fieldIndex = getFieldIndex(application, fieldName);
		for (ItemBadge itemBadge : items) {
			String value = (String) ((Map<String, String>) itemBadge.getFields().get(1).getValues().get(0).get("value"))
					.get("text");
			values.add(value);
		}
		return values;
	}

	public int getFieldIndex(Application application, String fieldName) {
		List<ApplicationField> fields = application.getFields();
		int index = 0;
		for (ApplicationField field : fields) {
			if (field.getConfiguration().getLabel().equals(fieldName)) {
				return index;
			}
			index++;
		}
		return -1;
	}

	public boolean validateWebHook(int hookId, String code) {
		try {
			HookAPI hookApi = factory.getAPI(HookAPI.class);
			hookApi.validateVerification(hookId, code);
			return true;
		} catch (APIApplicationException e) {
			return false;
		}
	}

	public Space getSpaceForItem(int itemId) {
		return spaceApi.getSpace(itemApi.getItem(itemId).getApplication().getSpaceId());
	}

	public Organization getOrgForSpace(Space space) {
		List<OrganizationWithSpaces> orgs = orgApi.getOrganizations();
		for (OrganizationWithSpaces org : orgs) {
			List<SpaceMini> spaces = org.getSpaces();
			for (SpaceMini spaceMini : spaces) {
				if (spaceMini.getId() == space.getId()) {
					return orgApi.getOrganization(org.getId());
				}
			}
		}
		return null;
	}

	public OrganizationWithSpaces getOrgWithSpaces(Organization org) {
		List<OrganizationWithSpaces> orgs = orgApi.getOrganizations();
		for (OrganizationWithSpaces orgSp : orgs) {
			if (orgSp.getId() == org.getId()) {
				return orgSp;
			}
		}
		return null;
	}

	public boolean hasSpace(Organization org, String targetWS) {
		OrganizationWithSpaces orgSpaces = getOrgWithSpaces(org);
		List<SpaceMini> spaces = orgSpaces.getSpaces();
		boolean found = false;
		for (SpaceMini spaceMini : spaces) {
			if (spaceMini.getName().equals(targetWS)) {
				return true;
			}
		}
		return false;
	}

	public void removeApps(Space space) {
		// space exists -> clean all apps
		List<Application> apps = appApi.getAppsOnSpace(space.getId());
		for (Application application : apps) {
			appApi.deleteApp(application.getId());
		}
	}

	public List<Space> findSpaceByName(String name) {
		List<Space> spaces = new ArrayList<Space>();
		List<OrganizationWithSpaces> orgs = getOrgApi().getOrganizations(); 
		for (OrganizationWithSpaces org: orgs) {
			List<SpaceMini> space = org.getSpaces();
			for (SpaceMini spaceMini : space) {
				if (spaceMini.getName().equals(name)) {
					spaces.add(spaceApi.getSpace(spaceMini.getId()));
				}
			}
		}
		return spaces;
	}

	public TextFieldSupport getTextField(FieldValuesView view) {
		return new TextFieldSupport(view);
	}

	public ReferenceFieldSupport getReferenceField(FieldValuesView view) {
		return new ReferenceFieldSupport(view);
	}

	public CategoryFieldSupport getCategoryField(FieldValuesView view) {
		return new CategoryFieldSupport(view);
	}

	public String getStringValue(Item item, String fieldName) {
		List<FieldValuesView> values = item.getFields();
		for (FieldValuesView fieldValuesView : values) {
			if (fieldValuesView.getLabel().equals(fieldName)) {
				switch (fieldValuesView.getType()) {
				case TEXT:
					return (String) fieldValuesView.getValues().get(0).get("value");
				case CATEGORY:
					return (String) ((Map<String, Object>) fieldValuesView.getValues().get(0).get("value")).get("text");
				case APP:
					List<Map<String, ?>> references = fieldValuesView.getValues();
					String result = null;
					for (Map<String, ?> map : references) {
						if (result == null) {
							result = "";
						} else {
							result += ",";
						}
						result += String.valueOf(((Map<String, Object>) map.get("value")).get("item_id"));
					}
					return result;
				default:
					logger.warn("unsupported Podio FieldType '{}'", fieldValuesView.getType());
					return null;
				} 
			}
		}
		return null;
	}

	public boolean getBooleanValue(Item item, String fieldName) {
		return getStringValue(item, fieldName).equalsIgnoreCase("true");
	}

	public List<String> getStringValues(Item item, String fieldName) {
		String values = getStringValue(item, fieldName);
		if (values == null) {
			return null;
		}
		return new ArrayList<String>(Arrays.asList(values.split(",")));
	}

	public int getIntValue(Item item, String fieldName) {
		String val = getStringValue(item, fieldName);
		if (val != null) {
			return Integer.parseInt(val);
		} else {
			return 0;
		}
	}

	public int getRefItemId(Item item, String fieldName) {
		return getIntValue(item, fieldName);
	}

	public List<Integer> getRefItemIds(Item item, String fieldName) {
		List<String> values = getStringValues(item, fieldName);
		List<Integer> ids = new ArrayList<Integer>();
		if (values != null) {
			for (String value : values) {
				int id = Integer.parseInt(value.trim());
				ids.add(id);
			}
		}
		return ids;
	}

	/**
	 * Retrieves all organizations from Podio that the user can create new spaces in.
	 * (A user can be part of more than one organization and he may not be able to create
	 * spaces in all of them.)
	 * 
	 * Uses one API call per Organization.
	 * 
	 * @return list of organizations
	 */
	public List<OrganizationWithSpaces> getWritableOrgs() {
		List<OrganizationWithSpaces> allOrgs = this.getOrgApi().getOrganizations();
		List<OrganizationWithSpaces> writableOrgs = new ArrayList<OrganizationWithSpaces>();
		for (OrganizationWithSpaces org: allOrgs) {
			if (org.canAddSpace()) {
				writableOrgs.add(org);
			}
		}
		return writableOrgs;
	}

	
	/**
	 * Returns the total number of items in an app.
	 * 
	 * Uses one API call.
	 * 
	 * @param appId
	 * @return
	 */
	public Integer getItemCount(int appId) {
		ItemsResponse response = this.getItems(appId, 1);
		return response.getTotal();
	}
	
	/**
	 * Get items from an app, up to a maximum of max.
	 * 
	 * Uses one API call.
	 * 
	 * @param appId the app to get the items from
	 * @param max the maximum number of items to get
	 * @return
	 */
	public ItemsResponse getItems(int appId, int max) {
		return this.getItemApi().getItems(appId, max, 0, null, false);
	}
}
