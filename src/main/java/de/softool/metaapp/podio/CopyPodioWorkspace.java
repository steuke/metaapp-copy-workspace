package de.softool.metaapp.podio;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CopyPodioWorkspace {

	private static final Logger logger = LoggerFactory.getLogger(CopyPodioWorkspace.class);
	
	PodioHelper podio;
	
	public CopyPodioWorkspace(PodioHelper podio) {
		this.podio = podio;
	}
	
	/**
	 * 
	 * @param space_id is the name of the workspace to copy
	 * @param target_space_name is the name of the new copied workspace
	 * @param org_id is the organization id where the target workspace should be created
	 * @return the target space id (i.e. the space id of the newly created workspace)
	 */
	public int copy(int space_id, String target_space_name, int org_id) {

		// TODO: Your code goes here
		
		
		// TODO: You need to replace the -1 and return the space id of the NEW workspace: 
		return -1;
	}
	
}
