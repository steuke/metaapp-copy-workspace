package de.softool.metaapp.podio;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.podio.app.Application;
import com.podio.app.ApplicationField;
import com.podio.app.ApplicationFieldConfiguration;
import com.podio.app.ApplicationFieldType;
import com.podio.item.ItemsResponse;

public class CopyPodioWorkspaceTest {

	private static final Logger logger = LoggerFactory.getLogger(CopyPodioWorkspaceTest.class);
	private final static String SOURCE_SPACE_NAME = "MetaApp Prototype (Test)";
	private final static String TARGET_SPACE_NAME = "MetaApp Prototype (Copy of Test)";


	@Test
	public void testCopyPodioWorkspace() {
		PodioHelper podio = PodioHelper.fromEnvironmentConfig();
		CopyPodioWorkspace cpw = new CopyPodioWorkspace(podio);
		// get id of first org that the user can create a workspace in
		int org_id = podio.getWritableOrgs().get(0).getId();
		// get space id for first space with the SOURCE_SPACE_NAME
		int source_space_id = podio.findSpaceByName(SOURCE_SPACE_NAME).get(0).getId();
		// make copy
		int target_space_id = cpw.copy(source_space_id, TARGET_SPACE_NAME, org_id);
		
		// run tests
		Assert.assertNotEquals("Your copy()-method must return the id of the newly created workspace.", -1, target_space_id); 
		Assert.assertTrue(isCopyOfOriginalSpace(podio, source_space_id, target_space_id));
	}




	/**
	 * Helper method that performs the actual test
	 * 
	 * @param podio
	 * @param source_space_id
	 * @param target_space_id
	 * @return
	 */
	private boolean isCopyOfOriginalSpace(PodioHelper podio, int source_space_id, int target_space_id) {
		List<Application> source_apps = podio.getAppApi().getAppsOnSpace(source_space_id);
		List<Application> target_apps = podio.getAppApi().getAppsOnSpace(target_space_id);

		StringBuilder msg = new StringBuilder(); // holds error messages

		// for each source app, check
		for (Application source_app_mini: source_apps) {
			
			// 1. does a target app exist with the name of the source app?
			String app_name = source_app_mini.getConfiguration().getName();
			Application target_app_mini = firstAppByName(target_apps, app_name);
			if (target_app_mini == null) {
				logger.error("Target workspace is missing app with name: " + app_name);
				return false;
			} else {
				logger.info("Found target app with name: " + app_name + ". Checking fields...");

				// get full app meta data (including fields)
				Application target_app = podio.getAppApi().getApp(target_app_mini.getId());
				Application source_app = podio.getAppApi().getApp(source_app_mini.getId());
				
				// 2. verify that target and source app have the same field name, types and references
				boolean fieldsMatch = hasSameAppFields(source_app, target_app, source_apps, target_apps);
				if (!fieldsMatch) {
					logger.error("   Fields do not match. See errors above.");
					return false;
				} else {
					logger.info("    OK. Fields match.");
				}
				
				// 3. verify that target and source app have the same number of items
				int source_item_count = podio.getItemCount(source_app.getId());
				int target_item_count = podio.getItemCount(target_app.getId());
				if (source_item_count != target_item_count) {
					logger.error("  Number of items do not match. Source app has " + source_item_count + " items, but target app has " + target_item_count);
					return false;
				} else {
					logger.info("    OK. Number of items match.");
				}
			}
		}
		return true;
	}

	/**
	 * Returns the first ApplicationField with the given label (i.e. name)
	 * @param app
	 * @param label
	 * @return
	 */
	private ApplicationField firstFieldByLabel(Application app, String label) {
		for (ApplicationField field: app.getFields()) {
			if (label.equals(field.getConfiguration().getLabel())) {
				return field;
			}
		}
		return null;
	}


	/**
	 * Compares source_app and target_app on a field-by-field basis. If all fields "match" (have the same
	 * label, type and in case of app ref. fiedls, also refernce the correct apps), return true. Otherwise
	 * returns false 
	 * 
	 * @param source_app
	 * @param target_app
	 * @param source_apps
	 * @param target_apps
	 * @return true if fields match, false otherwise
	 */
	private boolean hasSameAppFields(Application source_app,
			Application target_app, List<Application> source_apps, List<Application> target_apps) {
		boolean isSame = true;

		// test if we actually have fields to compare
		Assert.assertNotNull("Source app getFields() should not return null", source_app.getFields());
		Assert.assertNotNull("Target app getFields() should not return null", target_app.getFields());
		
		for (ApplicationField source_field: source_app.getFields()) {
			String label = source_field.getConfiguration().getLabel();
			ApplicationField target_field = firstFieldByLabel(target_app, label);

			if (target_field == null) {
				logger.error("  Target app is missing field with label: " + label);
				isSame = false;
			} else {
				logger.info("  Found target field with label: " + label);
				ApplicationFieldType source_type = source_field.getType();
				ApplicationFieldType target_type = target_field.getType();
				if (!target_type.equals(source_type)) {
					logger.error("  Field types do not match. Found '" + target_type.toString() 
							+ "' but expected '" + source_type.toString() + "'");
					isSame = false;
				} else {
					logger.info("  Field types match. Type: " + source_type.name());
					if ("APP".equals(source_type.name())) {
						assertReferencesAreCorrect(source_field.getConfiguration(), target_field.getConfiguration(), source_apps, target_apps);
						logger.info("    Ok. Referenceable apps for this field seem to match.");
					}
				}
			}

		}

		return isSame;
	}



	/**
	 * Check that the two app-reference fields (field1 and field2) "match" structurally, i.e. they reference apps with the same
	 * name in their own workspace.
	 * 
	 * This assumes that 
	 * - the target workspace does not contain multiple apps with the same name.
	 * - field1 is a field of an app from the source workspace
	 * - field2 is a field of an app from the target workspace
	 * 
	 * @return
	 */
	private void assertReferencesAreCorrect(ApplicationFieldConfiguration field1,
			ApplicationFieldConfiguration field2, List<Application> apps_from_source_space, List<Application> apps_from_target_space) {
		
		List<Integer> field1_referenceable_app_ids = field1.getSettings().getReferenceableTypes();
		List<Integer> field2_referenceable_app_ids = field2.getSettings().getReferenceableTypes();
		
		
		// 1. field1 and field2 should reference the same number of apps
		Assert.assertSame("Field1 and field2 must reference the same number of apps.", field1_referenceable_app_ids.size(), field2_referenceable_app_ids.size());
		
		// 2. check that referenced apps exist and names match for field1 and field2
		for (Integer field1_referenceable_app_id: field1_referenceable_app_ids) {
			// Field1: get referenceable app and its name from field1
			Application field1_ref_app = this.appById(apps_from_source_space, field1_referenceable_app_id);
			Assert.assertNotNull("Couldn't find referenceable app of field1 in source workspace. Is the reference field in the source app referencing an app outside the source workspace? (This does not work.)", field1_ref_app);
			String field1_ref_app_name = field1_ref_app.getConfiguration().getName();
			
			// Field2: get referenceable app by name from target workspace
			Application field2_ref_app = this.firstAppByName(apps_from_target_space, field1_ref_app_name);
			Assert.assertNotNull("Unable to find matching referenceable app in target workspace with app name: " + field1_ref_app_name, field2_ref_app);
			
			// Found it. Now check that field2 actually references this app
			boolean is_referenced_by_field2 = field2_referenceable_app_ids.contains(field2_ref_app.getId());
			Assert.assertTrue("Field2 must reference the app with the name '" + field1_ref_app_name + "' and id: " + field2_ref_app.getId(), is_referenced_by_field2);
		}
		
	}


	/**
	 * Get an app by its id from a list of apps.
	 * 
	 * @param apps
	 * @param id
	 * @return
	 */
	private Application appById(List<Application> apps, int id) {
		for (Application app: apps) {
			if (app.getId() == id) {
				return app;
			}
		}
		return null;
	}

	/**
	 * Get the first app with the given name from a list of apps.
	 * 
	 * @param apps
	 * @param name
	 * @return
	 */
	private Application firstAppByName(List<Application> apps,
			String name) {
		for (Application app: apps) {
			if (app.getConfiguration().getName().equals(name)) {
				return app;
			}
		}
		return null;
	}

}